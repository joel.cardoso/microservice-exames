package com.curso.microservices.app.exames.models.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.curso.microservices.commons.exames.models.entity.Exame;

@Repository
public interface ExameRepository extends PagingAndSortingRepository<Exame, Long>{
	
	@Query("select e from Exame e where e.nome like %?1%")
	public List<Exame> findByNome(String term);
}

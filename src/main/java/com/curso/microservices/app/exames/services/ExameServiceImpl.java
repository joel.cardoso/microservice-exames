package com.curso.microservices.app.exames.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.curso.microservices.app.exames.models.repository.CategoriaRepository;
import com.curso.microservices.app.exames.models.repository.ExameRepository;
import com.curso.microservices.commons.exames.models.entity.Categoria;
import com.curso.microservices.commons.exames.models.entity.Exame;
import com.curso.microservices.commons.services.CommonServiceImpl;

@Service
public class ExameServiceImpl extends CommonServiceImpl<Exame, ExameRepository> implements ExameService{
	
	@Autowired
	private CategoriaRepository categoriaRepository;

	@Transactional(readOnly = true)
	@Override
	public List<Exame> findByNome(String term) {
		return repository.findByNome(term);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Iterable<Categoria> findAllCategorias() {
		return categoriaRepository.findAll();
	}

}

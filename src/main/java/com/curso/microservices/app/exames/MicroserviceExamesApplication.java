package com.curso.microservices.app.exames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@EntityScan({"com.curso.microservices.commons.exames.models.entity"})
public class MicroserviceExamesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceExamesApplication.class, args);
	}

}

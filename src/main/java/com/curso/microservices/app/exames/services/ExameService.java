package com.curso.microservices.app.exames.services;

import java.util.List;

import com.curso.microservices.commons.exames.models.entity.Categoria;
import com.curso.microservices.commons.exames.models.entity.Exame;
import com.curso.microservices.commons.services.CommonService;

public interface ExameService extends CommonService<Exame>{
	
	public List<Exame> findByNome(String term);
	
	public Iterable<Categoria> findAllCategorias();
}

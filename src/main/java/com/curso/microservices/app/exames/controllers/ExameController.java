package com.curso.microservices.app.exames.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.curso.microservices.app.exames.services.ExameService;
import com.curso.microservices.commons.controllers.CommonController;
import com.curso.microservices.commons.exames.models.entity.Exame;
import com.curso.microservices.commons.exames.models.entity.Pergunta;

@RestController
public class ExameController extends CommonController<Exame, ExameService>{
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Exame exame, BindingResult result, @PathVariable Long id) {
		
		if (result.hasErrors()) {
			return this.validate(result);
		}
		
		Optional<Exame> optinalExame = service.findById(id);
		
		if (!optinalExame.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Exame exameSaved = optinalExame.get();
		exameSaved.setNome(exameSaved.getNome());
		
		// stream and filter JAVA 8
		List<Pergunta> eliminadas = exameSaved.getPerguntas()
		.stream()
		.filter(pdb -> !exame.getPerguntas().contains(pdb))
		.collect(Collectors.toList());
		
		eliminadas.forEach(exameSaved::removePerguntas);
		
		exameSaved.setPerguntas(exame.getPerguntas());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(exameSaved));
	}
	
	@GetMapping("/filter/{term}")
	public ResponseEntity<?> filterByNome(@PathVariable String term) {
		return ResponseEntity.ok(service.findByNome(term));
	}
	
	@GetMapping("/categorias")
	public ResponseEntity<?> findAllCategorias() {
		return ResponseEntity.ok(service.findAllCategorias());
	}
}
